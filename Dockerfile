# generating the package
FROM alpine:3.13.4 AS MAVEN_BUILD
WORKDIR /build
RUN apk add --no-cache maven
RUN apk add --no-cache openjdk11 --repository=http://dl-cdn.alpinelinux.org/alpine/edge/community
COPY pom.xml /build/
RUN mvn dependency:go-offline
COPY src /build/src/
RUN mvn package -DskipTests && \
    /usr/lib/jvm/default-jvm/bin/jlink \
    --verbose \
    --strip-debug \
    --module-path /usr/lib/jvm/default-jvm/jmods \
    --compress 2 \
    --add-modules jdk.unsupported,java.desktop,jdk.crypto.ec,java.sql,java.naming,java.management,java.instrument,java.security.jgss \
    --no-header-files \
    --no-man-pages \
    --output jdk-minimal

# building the layers
FROM adoptopenjdk/openjdk14:alpine-jre as LAYERS_BUILD
WORKDIR application
ARG JAR_FILE=/build/target/*.jar
COPY --from=MAVEN_BUILD ${JAR_FILE} application.jar
RUN java -Djarmode=layertools -jar application.jar extract

# building the image
FROM alpine:3.13.4
WORKDIR application
COPY --from=MAVEN_BUILD /build/jdk-minimal /opt/jdk/
COPY --from=LAYERS_BUILD application/dependencies/ ./
COPY --from=LAYERS_BUILD application/spring-boot-loader ./
COPY --from=LAYERS_BUILD application/snapshot-dependencies/ ./
COPY --from=LAYERS_BUILD application/application/ ./
ENV JAVA_HOME /opt/jdk
ENV PATH $PATH:/opt/jdk/bin
CMD /opt/jdk/bin/java org.springframework.boot.loader.JarLauncher
